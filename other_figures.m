clc; clear all;

%% Load Data

load('mayo_drfe_data.mat')
load('mayo_pnes_data.mat')
load('lemon_healthy_data.mat')
load('mayo_pnes_AED_data.mat')
load('mayo_pnes_noAED_data.mat')
load('lemon_blink_stats.mat')
load('mayo_blink_stats.mat')

%% Data organization
n_neg = size(all_lemon_features_ec, 1);
n_pos = size(all_mayo_features_ec, 1);
n_mh = size(all_mh_features_ec, 1);
n_mh_AED = size(all_mh_features_ec_AED, 1);
n_mh_nAED = size(all_mh_features_ec_nAED, 1);

regions = {'F.','T.','P.','O.'};
ranges = {'LA.','HA.'};
sides = {'L.','R.'};
[x,y,z] = meshgrid(1:2,1:4,1:2);
feature_reshape_order = strcat(sides(z(:)), ranges(x(:)), regions(y(:)));

h_features_ec = reshape(all_lemon_features_ec(:, 2:5, 3:4, 1:2), n_neg, 16);
h_features_eo = reshape(all_lemon_features_eo(:, 2:5, 3:4, 1:2), n_neg, 16);
e_features_ec = reshape(all_mayo_features_ec(:, 2:5, 3:4, 1:2), n_pos, 16);

mh_features_ec_AED = reshape(all_mh_features_ec_AED(:, 2:5, 3:4, 1:2), n_mh_AED, 16);
mh_features_ec_nAED = reshape(all_mh_features_ec_nAED(:, 2:5, 3:4, 1:2), n_mh_nAED, 16);

%% log transformation and CDF
figure;
histogram(h_features_ec(:,1), 40,'normalization','probability');
xlabel('Alpha power fraction')
ylabel('Probability')
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none'); findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);

figure;
histogram(log(h_features_ec(:,1)), 40,'normalization','probability');
xlim([-4 0])
xlabel('Logarithm of alpha power fraction')
ylabel('Probability')
a = [findall(gcf,'Marker','none'); findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);

figure;
[f,x] = ecdf(log(h_features_ec(:,1)));
plot(x,f);
xlim([-4 0])
xlabel('Logarithm of alpha power fraction')
ylabel('Cumulative Density')
a = [findall(gcf,'Marker','none'); findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);

%% Healthy EC vs EO CDFs

unique_lemon_ids = unique(lemon_ids);
n_test_healthy = 48;
n_cv = 5;
headplot_vals = zeros(1,8);

figure;
plot_index = reshape(1:8, 2, 4).';
for k = 1 : size(h_features_ec,2)/2
    
    curr_features_ec = (h_features_ec(:,k) + h_features_ec(:,8+k))/2;
    curr_features_eo = (h_features_eo(:,k) + h_features_eo(:,8+k))/2;
    x_vals = min(log(curr_features_eo),[],1):0.1:max(log(curr_features_ec),[],1);
    
    y_vals_eo = zeros(n_cv,length(x_vals));
    y_vals_ec = zeros(n_cv,length(x_vals));
    for cv = 1 : n_cv

        rand_ids = unique_lemon_ids(randperm(length(unique_lemon_ids)));
        train_lemon_ids = rand_ids(n_test_healthy+1:end);
        train_window_ids = ismember(lemon_ids, train_lemon_ids);
        
        curr_features_ec_tr = curr_features_ec(train_window_ids);
        curr_features_eo_tr = curr_features_eo(train_window_ids);

        pd = fitdist(log(curr_features_ec_tr), 'kernel');
        y_vals_ec(cv,:) = cdf(pd, x_vals);
        
        pd = fitdist(log(curr_features_eo_tr), 'kernel');
        y_vals_eo(cv,:) = cdf(pd, x_vals);
        
    end
    
    headplot_vals(k) = sum(mean(y_vals_eo)) - sum(mean(y_vals_ec));
    
    % figure;
    subplot(4,2,plot_index(k));

    
    shadedErrorBar(x_vals,mean(y_vals_ec),3*std(y_vals_ec),'lineprops','-b','transparent',true,'patchSaturation',0.2);
    hold on
    shadedErrorBar(x_vals,mean(y_vals_eo),3*std(y_vals_eo),'lineprops','-r','transparent',true,'patchSaturation',0.2);


    hold off
    box on
    %legend('EC', 'EO');
    ylim([0 1]);
    xlim([-4 0]);
    
    lineWidth = 2; lineCover=3*lineWidth;
    a = [findall(gcf,'Marker','none'); findall(gcf,'Marker','.')];
    set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);
    set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);
    
    
end

%% Headplot setup -- EEGLAB is required for plotting headplots.
eeglab;

%% produce healthy EC - EO headplots
headplot_vals = zeros(1,16);
for k = 1 : size(h_features_ec,2)
    
    curr_features_ec = h_features_ec(:,k);
    curr_features_eo = h_features_eo(:,k);
    x_vals = min(log(curr_features_eo),[],1):0.1:max(log(curr_features_ec),[],1);
    
    y_vals_eo = zeros(n_cv,length(x_vals));
    y_vals_ec = zeros(n_cv,length(x_vals));
    for cv = 1 : n_cv

        rand_ids = unique_lemon_ids(randperm(length(unique_lemon_ids)));
        train_lemon_ids = rand_ids(n_test_healthy+1:end);
        train_window_ids = ismember(lemon_ids, train_lemon_ids);
        
        curr_features_ec_tr = curr_features_ec(train_window_ids);
        curr_features_eo_tr = curr_features_eo(train_window_ids);

        pd = fitdist(log(curr_features_ec_tr), 'kernel');
        y_vals_ec(cv,:) = pdf(pd, x_vals);
        
        pd = fitdist(log(curr_features_eo_tr), 'kernel');
        y_vals_eo(cv,:) = pdf(pd, x_vals);
        
    end

    headplot_vals(k) = ws_distance(log(curr_features_eo),log(curr_features_ec),1);

end
headplot_vals_LA = headplot_vals([1:4, 9:12]);
headplot_vals_HA = headplot_vals([5:8, 13:16]);
figure; headplot(headplot_vals_LA,'splinefile', 'electrodes', 'on', 'view', 'top', 'maplimits', 'maxmin', 'cbar', 0); 
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);

figure; headplot(headplot_vals_HA,'splinefile', 'electrodes', 'on', 'view', 'top', 'maplimits', 'maxmin', 'cbar', 0);
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);

%% CDFs for Healthy vs Epilepsy

h_features = (h_features_ec(:,1:8) + h_features_ec(:,9:16))/2;
e_features = (e_features_ec(:,1:8) + e_features_ec(:,9:16))/2;

% h_features = (h_features_eo(:,1:8) + h_features_eo(:,9:16))/2;
% e_features = (e_features_eo(:,1:8) + e_features_eo(:,9:16))/2;
% 
% h_features = h_features_ec;
% e_features = e_features_ec;

unique_lemon_ids = unique(lemon_ids);
unique_mayo_ids = unique(mayo_ids);
n_test_healthy = 48;
n_test_epilepsy = 5;
n_cv = 10;

plot_index = reshape(1:8, 2, 4).';

figure;
for k = 1 : size(h_features,2)
    x_vals = min(log(e_features(e_features(:,k)>0,k)),[],1):0.1:max(log(h_features(:,k)),[],1);
    
    y_vals_h = zeros(n_cv,length(x_vals));
    y_vals_e = zeros(n_cv,length(x_vals));
    for cv = 1 : n_cv

        rand_ids = unique_lemon_ids(randperm(length(unique_lemon_ids)));
        train_lemon_ids = rand_ids(n_test_healthy+1:end);
        train_window_ids = ismember(lemon_ids, train_lemon_ids);
        h_features_train = h_features(train_window_ids, :);
        
        rand_ids = unique_mayo_ids(randperm(length(unique_mayo_ids)));
        train_mayo_ids = rand_ids(n_test_epilepsy+1:end);
        train_window_ids = ismember(mayo_ids, train_mayo_ids);
        e_features_train = e_features(train_window_ids, :);
        
        pd = fitdist(log(h_features_train(:,k)), 'kernel');
        y_vals_h(cv,:) = cdf(pd, x_vals);
        
        pd = fitdist(log(e_features_train(e_features_train(:,k)>0,k)), 'kernel');
        y_vals_e(cv,:) = cdf(pd, x_vals);
        
    end
    subplot(4,2,plot_index(k));
    
    shadedErrorBar(x_vals,mean(y_vals_h),3*std(y_vals_h),'lineprops','-b','transparent',true,'patchSaturation',0.2);
    hold on
    shadedErrorBar(x_vals,mean(y_vals_e),3*std(y_vals_e),'lineprops','-r','transparent',true,'patchSaturation',0.2);

    hold off
    box on
    ylim([0 1]);
    % xlim([-5 0]);
    
    lineWidth = 2; lineCover=3*lineWidth;
    a = [findall(gcf,'Marker','none'); findall(gcf,'Marker','.')];
    set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);
    set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);   
end

%% Headplots Healthy vs Epilepsy 

h_features = h_features_ec;
e_features = e_features_ec;

headplot_vals = zeros(1,size(h_features,2));
n_test_healthy = 0;
n_test_epilepsy = 0;
n_cv = 10;

for k = 1 : size(h_features,2)
    x_vals = min(log(e_features(e_features(:,k)>0,k)),[],1):0.1:max(log(h_features(:,k)),[],1);
    
    y_vals_h = zeros(n_cv,length(x_vals));
    y_vals_e = zeros(n_cv,length(x_vals));
    for cv = 1 : n_cv

        rand_ids = unique_lemon_ids(randperm(length(unique_lemon_ids)));
        train_lemon_ids = rand_ids(n_test_healthy+1:end);
        train_window_ids = ismember(lemon_ids, train_lemon_ids);
        h_features_train = h_features(train_window_ids, :);
        
        rand_ids = unique_mayo_ids(randperm(length(unique_mayo_ids)));
        train_mayo_ids = rand_ids(n_test_epilepsy+1:end);
        train_window_ids = ismember(mayo_ids, train_mayo_ids);
        e_features_train = e_features(train_window_ids, :);
        
        pd = fitdist(log(h_features_train(:,k)), 'kernel');
        y_vals_h(cv,:) = cdf(pd, x_vals);
        
        pd = fitdist(log(e_features_train(e_features_train(:,k)>0,k)), 'kernel');
        y_vals_e(cv,:) = cdf(pd, x_vals);
        
    end
    
    % headplot_vals(k) = sum(mean(y_vals_e)) - sum(mean(y_vals_h));
    headplot_vals(k) = ws_distance(mean(y_vals_h),mean(y_vals_e),1);
end

headplot_vals_LA = headplot_vals([1:4, 9:12]);
headplot_vals_HA = headplot_vals([5:8, 13:16]);

figure; headplot(headplot_vals_LA,'splinefile', 'electrodes', 'on', 'view', 'top', 'maplimits', 'maxmin', 'cbar', 0); 
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2); 

figure; headplot(headplot_vals_HA,'splinefile', 'electrodes', 'on', 'view', 'top', 'maplimits', 'maxmin', 'cbar', 0);
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2); 

%% Healthy vs Epilepsy BH-CDF based

h_features = h_features_ec;
e_features = e_features_ec;

% h_features = [h_features_ec h_features_eo];
% e_features = [e_features_ec e_features_eo];

cdf_vals_e = zeros(size(e_features));
cdf_vals_h = zeros(size(h_features));

for k = 1:size(e_features,2)
    [f,x] = ecdf(h_features(:,k));

    for i = 1 : size(e_features,1)
        [minValue,closestIndex] = min(abs(x-e_features(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_e(i,k) = f(closestIndex);
    end

    for i = 1 : size(h_features,1)
        [minValue,closestIndex] = min(abs(x-h_features(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_h(i,k) = f(closestIndex);
    end

end

h_probs = exp(mean(log(cdf_vals_h),2));
e_probs = exp(mean(log(cdf_vals_e),2));

figure;
groups = [h_probs' e_probs'];
index_group = [zeros(1,size(h_probs,1)) ones(1,size(e_probs,1))];
boxplot(groups, index_group, 'Labels', {'Healthy', 'DRFE'}, 'Whisker', 3);
ylim([0 1]);

set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none') findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);

%% Healthy (young and old separate) vs Epilepsy BH-CDF based

h_features = h_features_ec;
e_features = e_features_ec;

cdf_vals_e = zeros(size(e_features));
cdf_vals_h_o = zeros(sum(lemon_ages == 0), size(h_features,2));
cdf_vals_h_y = zeros(sum(lemon_ages == 1), size(h_features,2));

h_features_y = h_features(lemon_ages == 1,:);
h_features_o = h_features(lemon_ages == 0,:);

for k = 1:size(e_features,2)
    [f,x] = ecdf(h_features(:,k));

    for i = 1 : size(e_features,1)
        [minValue,closestIndex] = min(abs(x-e_features(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_e(i,k) = f(closestIndex);
    end

    for i = 1 : size(h_features_o,1)
        [minValue,closestIndex] = min(abs(x-h_features_o(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_h_o(i,k) = f(closestIndex);
    end
    
    for i = 1 : size(h_features_y,1)
        [minValue,closestIndex] = min(abs(x-h_features_y(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_h_y(i,k) = f(closestIndex);
    end

end

h_probs_y = exp(mean(log(cdf_vals_h_y),2));
h_probs_o = exp(mean(log(cdf_vals_h_o),2));
e_probs = exp(mean(log(cdf_vals_e),2));

figure;
groups = [h_probs_y' h_probs_o' e_probs'];
index_group = [zeros(1,size(h_probs_y,1)) ones(1,size(h_probs_o,1)) 2*ones(1,size(e_probs,1))];
boxplot(groups, index_group, 'Labels', {'Healthy-Y', 'Healthy-O', 'DRFE'}, 'Whisker', 3);
ylim([0 1]);

set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none') findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);
% ANOVA
figure;
index_group = [zeros(1,size(h_probs_y,1)) ones(1,size(h_probs_o,1)) 2*ones(1,size(e_probs,1))];
index_group(index_group == 0) = 3;
index_group(index_group == 2) = 0;
index_group(index_group == 1) = 2;
[p,tbl,stats] = anova1(groups, index_group, 'off');
[c,m,h]=multcompare(stats);
set(gca,'YTickLabel',{'Healthy-Y', 'Healthy-O', 'DRFE'});
%xlim([0.1,0.5]);
xlabel('P(Normal)');
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none') findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);
view([90,-90])

%% CDFs for Seizure Focus localization

h_features = (h_features_ec(:,1:8) + h_features_ec(:,9:16))/2;
e_features = (e_features_ec(:,1:8) + e_features_ec(:,9:16))/2;

% h_features = (h_features_eo(:,1:8) + h_features_eo(:,9:16))/2;
% e_features = (e_features_eo(:,1:8) + e_features_eo(:,9:16))/2;

plot_index = reshape(1:8, 2, 4).';

e_features_l = e_features(left_ids == 1,:);
e_features_r = e_features(right_ids == 1,:);

headplot_vals = zeros(1,size(h_features,2));
figure;
for k = 1 : size(h_features,2)
    x_vals = min(log(e_features(e_features(:,k)>0,k)),[],1):0.1:max(log(h_features(:,k)),[],1);
    
    pd = fitdist(log(e_features_l(e_features_l(:,k)>0,k)), 'kernel');
    y_vals_el = cdf(pd, x_vals);

    pd = fitdist(log(e_features_r(e_features_r(:,k)>0,k)), 'kernel');
    y_vals_er = cdf(pd, x_vals);
    
    subplot(4,2,plot_index(k));
    
    plot(x_vals,y_vals_er,'-b');
    hold on
    plot(x_vals,y_vals_el,'-r');

    hold off
    box on
    ylim([0 1]);
    % xlim([-5 0]);
    
    lineWidth = 2; lineCover=3*lineWidth;
    a = [findall(gcf,'Marker','none'); findall(gcf,'Marker','.')];
    set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);
    set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2); 
end

%% Headplots Based on Seizure Focus Side 

h_features = h_features_ec;
e_features = e_features_ec;

e_features_l = e_features(left_ids == 1,:);
e_features_r = e_features(right_ids == 1,:);

headplot_vals = zeros(1,size(h_features,2));

for k = 1 : size(h_features,2)
    x_vals = min(log(e_features(e_features(:,k)>0,k)),[],1):0.1:max(log(h_features(:,k)),[],1);

    pd = fitdist(log(e_features_l(e_features_l(:,k)>0,k)), 'kernel');
    y_vals_el = cdf(pd, x_vals);

    pd = fitdist(log(e_features_r(e_features_r(:,k)>0,k)), 'kernel');
    y_vals_er = cdf(pd, x_vals);
    
    headplot_vals(k) = ws_distance(y_vals_el,y_vals_er,1);
    
end

headplot_vals_LA = headplot_vals([1:4, 9:12]);
headplot_vals_HA = headplot_vals([5:8, 13:16]);

figure; headplot(headplot_vals_LA,'splinefile', 'electrodes', 'on', 'view', 'top', 'maplimits', 'maxmin', 'cbar', 0); 
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2); 

figure; headplot(headplot_vals_HA,'splinefile', 'electrodes', 'on', 'view', 'top', 'maplimits', 'maxmin', 'cbar', 0);
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2); 

%% Seizure Focus Lateralization -- Right-handed -- BH-CDF Based

h_features = h_features_ec;
e_features = e_features_ec;

% h_features = [h_features_ec h_features_eo];
% e_features = [e_features_ec e_features_eo];

cdf_vals_h = zeros(size(h_features));
cdf_vals_e_l = zeros(sum(left_ids == 1), size(e_features,2));
cdf_vals_e_r = zeros(sum(right_ids == 1), size(e_features,2));

e_features_l = e_features(left_ids == 1,:);
e_features_r = e_features(right_ids == 1,:);

for k = 1:size(e_features,2)
    [f,x] = ecdf(h_features(:,k));

    for i = 1 : size(h_features,1)
        [minValue,closestIndex] = min(abs(x-h_features(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_h(i,k) = f(closestIndex);
    end

    for i = 1 : size(e_features_l,1)
        [minValue,closestIndex] = min(abs(x-e_features_l(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_e_l(i,k) = f(closestIndex);
    end
    
    for i = 1 : size(e_features_r,1)
        [minValue,closestIndex] = min(abs(x-e_features_r(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_e_r(i,k) = f(closestIndex);
    end

end

h_probs = exp(mean(log(cdf_vals_h),2));
e_probs_l = exp(mean(log(cdf_vals_e_l),2));
e_probs_r = exp(mean(log(cdf_vals_e_r),2));

figure;
groups = [h_probs' e_probs_r' e_probs_l'];
index_group = [zeros(1,size(h_probs,1)) ones(1,size(e_probs_r,1)) 2*ones(1,size(e_probs_l,1))];
boxplot(groups, index_group, 'Labels', {'Healthy', 'SF-R-T', 'SF-L-T'}, 'Whisker', 3);
ylim([0 1]);

set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none') findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);

%% Drugs in epilepsy

h_features = h_features_ec;
e_features = e_features_ec;

% h_features = [h_features_ec h_features_eo];
% e_features = [e_features_ec e_features_eo];

cdf_vals_h = zeros(size(h_features));
cdf_vals_e_drug = zeros(sum(drug_counts > 0), size(e_features,2));
cdf_vals_e_no_drug = zeros(sum(drug_counts == 0), size(e_features,2));

e_features_drug = e_features(drug_counts > 0,:);
e_features_no_drug = e_features(drug_counts == 0,:);

for k = 1:size(h_features,2)
    [f,x] = ecdf(h_features(:,k));

    for i = 1 : size(h_features,1)
        [minValue,closestIndex] = min(abs(x-h_features(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_h(i,k) = f(closestIndex);
    end

    for i = 1 : size(e_features_drug,1)
        [minValue,closestIndex] = min(abs(x-e_features_drug(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_e_drug(i,k) = f(closestIndex);
    end
    
    for i = 1 : size(e_features_no_drug,1)
        [minValue,closestIndex] = min(abs(x-e_features_no_drug(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_e_no_drug(i,k) = f(closestIndex);
    end

end

h_probs = exp(mean(log(cdf_vals_h),2));
e_probs_drug = exp(mean(log(cdf_vals_e_drug),2));
e_probs_no_drug = exp(mean(log(cdf_vals_e_no_drug),2));

figure;
groups = [h_probs' e_probs_no_drug' e_probs_drug'];
index_group = [zeros(1,size(h_probs,1)) ones(1,size(e_probs_no_drug,1)) 2*ones(1,size(e_probs_drug,1))];
boxplot(groups, index_group, 'Labels', {'Healthy', 'Drugs=0', 'Drugs>0'}, 'Whisker', 3);
ylim([0 1]);

set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none') findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);

% ANOVA
index_group = [zeros(1,size(h_probs,1)) ones(1,size(e_probs_no_drug,1)) 2*ones(1,size(e_probs_drug,1))];
index_group(index_group == 0) = 3;
index_group(index_group == 2) = 0;
index_group(index_group == 1) = 2;
[p,tbl,stats] = anova1(groups, index_group, 'off');
[c,m,h]=multcompare(stats);
set(gca,'YTickLabel',{'Healthy', 'AED=0', 'AED>0'});
xlim([0.2,0.5]);
xlabel('P(Normal)');
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none') findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);
view([90,-90])

%% levetiracetam vs lamotrigine

h_features = h_features_ec;
e_features = e_features_ec;

cdf_vals_h = zeros(size(h_features));
cdf_vals_e_levet = zeros(sum(levet_taker), size(e_features,2));
cdf_vals_e_lamot = zeros(sum(lamot_taker), size(e_features,2));

e_features_levet = e_features(levet_taker==1,:);
e_features_lamot = e_features(lamot_taker==1,:);

for k = 1:size(h_features,2)
    [f,x] = ecdf(h_features(:,k));

    for i = 1 : size(h_features,1)
        [minValue,closestIndex] = min(abs(x-h_features(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_h(i,k) = f(closestIndex);
    end

    for i = 1 : size(e_features_levet,1)
        [minValue,closestIndex] = min(abs(x-e_features_levet(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_e_levet(i,k) = f(closestIndex);
    end
    
    for i = 1 : size(e_features_lamot,1)
        [minValue,closestIndex] = min(abs(x-e_features_lamot(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_e_lamot(i,k) = f(closestIndex);
    end

end

h_probs = exp(mean(log(cdf_vals_h),2));
e_probs_levet = exp(mean(log(cdf_vals_e_levet),2));
e_probs_lamot = exp(mean(log(cdf_vals_e_lamot),2));

figure;
groups = [h_probs' e_probs_levet' e_probs_lamot'];
index_group = [zeros(1,size(h_probs,1)) ones(1,size(e_probs_levet,1)) 2*ones(1,size(e_probs_lamot,1))];
boxplot(groups, index_group, 'Labels', {'Healthy', 'LEVET', 'LAMOT'}, 'Whisker', 3);
ylim([0 1]);

set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none') findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);

% ANOVA
figure;
index_group = [zeros(1,size(h_probs,1)) ones(1,size(e_probs_levet,1)) 2*ones(1,size(e_probs_lamot,1))];
index_group(index_group == 0) = 3;
index_group(index_group == 2) = 0;
index_group(index_group == 1) = 2;
[p,tbl,stats] = anova1(groups, index_group, 'off');
[c,m,h]=multcompare(stats);
set(gca,'YTickLabel',{'Healthy', 'LEVET', 'LAMOT'});
%xlim([0.1,0.5]);
xlabel('P(Normal)');
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none') findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);
view([90,-90])

%% Box plots LEMON Vs. MH Vs. Mayo

% lemon_features = [h_features_ec h_features_eo];
% mayo_features = [e_features_ec e_features_eo];
% mh_features = [mh_features_ec mh_features_eo];

lemon_features = h_features_ec;
mayo_features = e_features_ec;
mh_features = mh_features_ec;

cdf_vals_mayo = zeros(size(mayo_features));
cdf_vals_lemon = zeros(size(lemon_features));
cdf_vals_mh = zeros(size(mh_features));

for k = 1:size(lemon_features,2)
    [f,x] = ecdf(lemon_features(:,k));

    for i = 1 : size(mayo_features,1)
        [minValue,closestIndex] = min(abs(x-mayo_features(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_mayo(i,k) = f(closestIndex);
    end

    for i = 1 : size(lemon_features,1)
        [minValue,closestIndex] = min(abs(x-lemon_features(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_lemon(i,k) = f(closestIndex);
    end
    
    for i = 1 : size(mh_features,1)
        [minValue,closestIndex] = min(abs(x-mh_features(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_mh(i,k) = f(closestIndex);
    end

end

lemon_probs = exp(mean(log(cdf_vals_lemon),2));
mh_probs = exp(mean(log(cdf_vals_mh),2));
mayo_probs = exp(mean(log(cdf_vals_mayo),2));

figure;
groups = [lemon_probs' mh_probs' mayo_probs'];
index_group = [zeros(1,size(lemon_probs,1)) ones(1,size(mh_probs,1)) 2*ones(1,size(mayo_probs,1))];
boxplot(groups, index_group, 'Labels', {'LEMON', 'Mayo-H', 'Mayo'}, 'Whisker', 3);
ylim([0 1]);

set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none') findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);
% ANOVA
figure;
index_group = [zeros(1,size(lemon_probs,1)) ones(1,size(mh_probs,1)) 2*ones(1,size(mayo_probs,1))];
index_group(index_group == 0) = 3;
index_group(index_group == 2) = 0;
index_group(index_group == 1) = 2;
[p,tbl,stats] = anova1(groups, index_group, 'off');
[c,m,h]=multcompare(stats);
set(gca,'YTickLabel',{'LEMON', 'Mayo-H', 'Mayo'});
%xlim([0.1,0.5]);
xlabel('Means');
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none') findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);
view([90,-90])

%% Arousal state comparison

lemon_blink_counts = [];
for i = 1 : length(lemon_blink_stats)
    if(~isempty(lemon_blink_stats(i).blinksPerMin))
        % lemon_blink_counts = [lemon_blink_counts; lemon_blink_stats(i).numberGoodBlinks*60/lemon_blink_stats(i).seconds];
        % lemon_blink_counts = [lemon_blink_counts; lemon_blink_stats(i).blinksPerMin(1)];
        lemon_blink_counts = [lemon_blink_counts; lemon_blink_stats(i).blinksPerMin(5)];
    end
end

mayo_blink_counts = [];
for i = 1 : length(mayo_blink_stats)
    if(~isempty(mayo_blink_stats(i).blinksPerMin))
        % mayo_blink_counts = [mayo_blink_counts; mayo_blink_stats(i).numberGoodBlinks*60/mayo_blink_stats(i).seconds];
        % mayo_blink_counts = [mayo_blink_counts; mayo_blink_stats(i).blinksPerMin(1)];
        mayo_blink_counts = [mayo_blink_counts; mayo_blink_stats(i).blinksPerMin(5)];
    end
end

% blinkStatistics.numberBlinks*60/blinkStatistics.seconds;

% ANOVA
groups = [lemon_blink_counts; mayo_blink_counts]';
index_group = [zeros(1,size(lemon_blink_counts,1)) ones(1,size(mayo_blink_counts,1))];

index_group(index_group == 0) = 2;
index_group(index_group == 1) = 0;

figure;
[p,tbl,stats] = anova1(groups, index_group, 'off');
[c,m,h]=multcompare(stats);
set(gca,'YTickLabel',{'Healthy', 'DRFE'});
% xlim([12,18]);
xlabel('# eye blinks/min');
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none') findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);
view([90,-90])

%% AEDs in PNES vs DRFE

h_features = h_features_ec;
e_features = e_features_ec;
mh_features_drug = mh_features_ec_AED;

% 30, 36, 42, 45, 50
cdf_vals_h = zeros(size(h_features));
cdf_vals_e_drug = zeros(sum(drug_counts > 0), size(e_features,2));
cdf_vals_mh_drug = zeros(size(mh_features_drug));

e_features_drug = e_features(drug_counts > 0,:);

for k = 1:size(h_features,2)
    [f,x] = ecdf(h_features(:,k));

    for i = 1 : size(h_features,1)
        [minValue,closestIndex] = min(abs(x-h_features(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_h(i,k) = f(closestIndex);
    end

    for i = 1 : size(e_features_drug,1)
        [minValue,closestIndex] = min(abs(x-e_features_drug(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_e_drug(i,k) = f(closestIndex);
    end
    
    for i = 1 : size(mh_features_drug,1)
        [minValue,closestIndex] = min(abs(x-mh_features_drug(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_mh_drug(i,k) = f(closestIndex);
    end

end

h_probs = exp(mean(log(cdf_vals_h),2));
e_probs_drug = exp(mean(log(cdf_vals_e_drug),2));
mh_probs_drug = exp(mean(log(cdf_vals_mh_drug),2));

figure;
groups = [h_probs' mh_probs_drug' e_probs_drug'];
index_group = [zeros(1,size(h_probs,1)) ones(1,size(mh_probs_drug,1)) 2*ones(1,size(e_probs_drug,1))];
boxplot(groups, index_group, 'Labels', {'Healthy', 'PNES-AEDs', 'DRFE-AEDs'}, 'Whisker', 3);
ylim([0 1]);

set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none') findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);

% ANOVA
index_group = [zeros(1,size(h_probs,1)) ones(1,size(mh_probs_drug,1)) 2*ones(1,size(e_probs_drug,1))];
index_group(index_group == 0) = 3;
index_group(index_group == 2) = 0;
index_group(index_group == 1) = 2;
[p,tbl,stats] = anova1(groups, index_group, 'off');
[c,m,h]=multcompare(stats);
set(gca,'YTickLabel',{'Healthy', sprintf('%s\\newline%s', 'AED>0', 'PNES'), sprintf('%s\\newline%s', 'AED>0', 'DRFE')});
xlim([0.2,0.5]);
xlabel('P(Normal)');
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none') findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);
view([90,-90])

%% PNES AEDs vs No-AEDs

h_features = h_features_ec;
mh_features_drug = mh_features_ec_AED;
mh_features_no_drug = mh_features_ec_nAED;

% 30, 36, 42, 45, 50
cdf_vals_h = zeros(size(h_features));
cdf_vals_mh_drug = zeros(size(mh_features_drug));
cdf_vals_mh_no_drug = zeros(size(mh_features_no_drug));

for k = 1:size(h_features,2)
    [f,x] = ecdf(h_features(:,k));

    for i = 1 : size(h_features,1)
        [minValue,closestIndex] = min(abs(x-h_features(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_h(i,k) = f(closestIndex);
    end

    for i = 1 : size(mh_features_no_drug,1)
        [minValue,closestIndex] = min(abs(x-mh_features_no_drug(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_mh_no_drug(i,k) = f(closestIndex);
    end
    
    for i = 1 : size(mh_features_drug,1)
        [minValue,closestIndex] = min(abs(x-mh_features_drug(i,k)));
        if(f(closestIndex) == 0)
            closestIndex = 2;
        end
        cdf_vals_mh_drug(i,k) = f(closestIndex);
    end

end

h_probs = exp(mean(log(cdf_vals_h),2));
mh_probs_no_drug = exp(mean(log(cdf_vals_mh_no_drug),2));
mh_probs_drug = exp(mean(log(cdf_vals_mh_drug),2));

figure;
groups = [h_probs' mh_probs_no_drug' mh_probs_drug'];
index_group = [zeros(1,size(h_probs,1)) ones(1,size(mh_probs_no_drug,1)) 2*ones(1,size(mh_probs_drug,1))];
boxplot(groups, index_group, 'Labels', {'Healthy', 'PNES-No-AEDs', 'PNES-AEDs'}, 'Whisker', 3);
ylim([0 1]);

set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none') findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);

% ANOVA
index_group = [zeros(1,size(h_probs,1)) ones(1,size(mh_probs_no_drug,1)) 2*ones(1,size(mh_probs_drug,1))];
index_group(index_group == 0) = 3;
index_group(index_group == 2) = 0;
index_group(index_group == 1) = 2;
[p,tbl,stats] = anova1(groups, index_group, 'off');
[c,m,h]=multcompare(stats);
set(gca,'YTickLabel',{'Healthy', sprintf('%s\\newline%s', 'AED=0', 'PNES'), sprintf('%s\\newline%s', 'AED>0', 'PNES')});
xlim([0.2,0.5]);
xlabel('P(Normal)');
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none') findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);
view([90,-90])
