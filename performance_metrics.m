function [gof_st_ep, gof_st_ep_pat, gof_st_soz, gof_st_soz_pat] = performance_metrics(h_probs, e_probs, h_ids, e_ids, left_ids, right_ids)

    all_labels = [-1*ones(length(h_probs),1); ones(length(e_probs),1)];
    all_probs = [h_probs; e_probs];
    
    gof_st_ep = goodness_of_fit_metrics(all_labels, all_probs, all_probs, 0);
    
    soz_labels = [-1*ones(sum(right_ids==1),1); ones(sum(left_ids==1),1)];
    soz_probs = [e_probs(right_ids==1); e_probs(left_ids==1)];
    
    gof_st_soz = goodness_of_fit_metrics(soz_labels, soz_probs, soz_probs, 0);

    all_ids = [h_ids; e_ids];
    unique_ids = unique(all_ids);
    pat_labels = [];
    pat_probs = [];
    
    for i = 1 : length(unique_ids)
        curr_id = unique_ids(i);
        curr_labels = all_labels(all_ids == curr_id);
        curr_probs = all_probs(all_ids == curr_id);
        pat_labels(i) = curr_labels(1);
        pat_probs(i) = mean(curr_probs);
    end
    gof_st_ep_pat = goodness_of_fit_metrics(pat_labels, pat_probs, pat_probs, 0);
    
    unique_ids = unique([e_ids(right_ids==1); e_ids(left_ids==1)]);
    pat_labels = [];
    pat_probs = [];
    
    for i = 1 : length(unique_ids)
        curr_id = unique_ids(i);
        
        curr_probs = e_probs(e_ids == curr_id);
        if(ismember(curr_id, e_ids(right_ids==1)))
            pat_labels(i) = -1;
        elseif(ismember(curr_id, e_ids(left_ids==1)))
            pat_labels(i) = 1;            
        end
        pat_probs(i) = mean(curr_probs);
    end
    gof_st_soz_pat = goodness_of_fit_metrics(pat_labels, pat_probs, pat_probs, 0);
end