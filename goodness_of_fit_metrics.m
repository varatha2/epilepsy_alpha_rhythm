function gof_struct = goodness_of_fit_metrics(true_labels, pred_labels, probs, threshold_percentile)

    if(size(true_labels, 1) ~= size(probs, 1))
        probs = probs';
    end
    
    [X,Y,T,AUC,OPTROCPT] = perfcurve(true_labels,probs,1,'XVals',0:0.05:1,'UseNearest','off');
    threshold_opt = T((X==OPTROCPT(1))&(Y==OPTROCPT(2)));
    
    pred_labels = double(probs > threshold_opt);
%     pred_labels = double(probs > 0.5);
    pred_labels(pred_labels == 0) = -1;
    
    accuracy=100*mean(pred_labels==true_labels);
    Sn = 100*sum(pred_labels==1 & true_labels == 1) / sum(true_labels == 1);
    Sp = 100*sum(pred_labels==-1 & true_labels == -1) / sum(true_labels == -1);
    
    recall = Sn;
    precision = 100*sum(pred_labels==1 & true_labels == 1) / sum(pred_labels == 1);
    F1 = 2 * precision * recall /(precision + recall);
    
    gof_struct.AUC = AUC;
    gof_struct.X = X;
    gof_struct.Y = Y;
    gof_struct.Sn = Sn;
    gof_struct.Sp = Sp;
    gof_struct.accuracy = accuracy;
    gof_struct.precision = precision;
    gof_struct.recall = recall;
    gof_struct.F1 = F1;
end