This repo contains code and deidentified data to reproduce results presented in 
Varatharajah, Yogatheesan, et al. "Characterizing the Electrophysiological Abnormalities in Visually-reviewed Normal EEGs of Drug-Resistant Focal Epilepsy Patients." Brain communications 2021.

MATLAB and EEGLAB are required.

1. Main classification results can be produced using classification_results.m
2. Other figures can be reproduced using other_figures.m
