clc; clear all;

%% Load Data

load('mayo_drfe_data.mat')
load('lemon_healthy_data.mat')

%% CDF-based BH model
n_neg = size(all_lemon_features_ec, 1);
n_pos = size(all_mayo_features_ec, 1);

all_wind_patient_ids = [lemon_ids; mayo_ids];
all_patient_ids = unique(all_wind_patient_ids);
all_labels = [-1*ones(n_neg,1); ones(n_pos,1)];
unique_lemon_ids = unique(lemon_ids);

n_test_healthy = 48;

%% All regions

h_features = reshape(all_lemon_features_ec(:, 2:5, 3:4, 1:2), n_neg, 16);
e_features = reshape(all_mayo_features_ec(:, 2:5, 3:4, 1:2), n_pos, 16);

% h_features = reshape(all_lemon_features_ec(:, 2:5, 4, 1:2), n_neg, 8);
% e_features = reshape(all_mayo_features_ec(:, 2:5, 4, 1:2), n_pos, 8);

for cv = 1:10

    rand_ids = unique_lemon_ids(randperm(length(unique_lemon_ids)));
    train_lemon_ids = rand_ids(n_test_healthy+1:end);
    test_lemon_ids = rand_ids(1:n_test_healthy);

    train_window_ids = ismember(lemon_ids, train_lemon_ids);
    test_window_ids = ismember(lemon_ids, test_lemon_ids);

    h_features_train = h_features(train_window_ids, :);
    h_features_test = h_features(test_window_ids, :);
    
    cdf_vals_e = zeros(size(e_features));
    cdf_vals_h_train = zeros(size(h_features_train));
    cdf_vals_h_test = zeros(size(h_features_test));
    for k = 1:size(cdf_vals_h_test,2)
        [f,x] = ecdf(h_features_train(:,k));
        
        for i = 1 : size(e_features,1)
            [minValue,closestIndex] = min(abs(x-e_features(i,k)));
            if(f(closestIndex) == 0)
                closestIndex = 2;
            end
            cdf_vals_e(i,k) = f(closestIndex);
        end
        
        for i = 1 : size(h_features_train,1)
            [minValue,closestIndex] = min(abs(x-h_features_train(i,k)));
            if(f(closestIndex) == 0)
                closestIndex = 2;
            end
            cdf_vals_h_train(i,k) = f(closestIndex);
        end
        
        for i = 1 : size(h_features_test,1)
            [minValue,closestIndex] = min(abs(x-h_features_test(i,k)));
            if(f(closestIndex) == 0)
                closestIndex = 2;
            end
            cdf_vals_h_test(i,k) = f(closestIndex);
        end
    end

    h_bhd_vals = 1-exp(mean(log(cdf_vals_h_test),2));
    e_bhd_vals = 1-exp(mean(log(cdf_vals_e),2));
    
    [gof_st_ep_all(cv), gof_st_ep_pat_all(cv), gof_st_soz_all(cv), gof_st_soz_pat_all(cv)] = ...
        performance_metrics(h_bhd_vals, e_bhd_vals, lemon_ids(test_window_ids), mayo_ids, left_ids, right_ids);
end

%% Frontal and Temporal

h_features = reshape(all_lemon_features_ec(:, 2:3, 3:4, 1:2), n_neg, 8);
e_features = reshape(all_mayo_features_ec(:, 2:3, 3:4, 1:2), n_pos, 8);

% h_features = reshape(all_lemon_features_ec(:, 2:3, 4, 1:2), n_neg, 4);
% e_features = reshape(all_mayo_features_ec(:, 2:3, 4, 1:2), n_pos, 4);

for cv = 1:10
    rand_ids = unique_lemon_ids(randperm(length(unique_lemon_ids)));
    train_lemon_ids = rand_ids(n_test_healthy+1:end);
    test_lemon_ids = rand_ids(1:n_test_healthy);

    train_window_ids = ismember(lemon_ids, train_lemon_ids);
    test_window_ids = ismember(lemon_ids, test_lemon_ids);

    h_features_train = h_features(train_window_ids, :);
    h_features_test = h_features(test_window_ids, :);
    
    cdf_vals_e = zeros(size(e_features));
    cdf_vals_h_train = zeros(size(h_features_train));
    cdf_vals_h_test = zeros(size(h_features_test));
    for k = 1:size(cdf_vals_h_test,2)
        [f,x] = ecdf(h_features_train(:,k));
        
        for i = 1 : size(e_features,1)
            [minValue,closestIndex] = min(abs(x-e_features(i,k)));
            if(f(closestIndex) == 0)
                closestIndex = 2;
            end
            cdf_vals_e(i,k) = f(closestIndex);
        end
        
        for i = 1 : size(h_features_train,1)
            [minValue,closestIndex] = min(abs(x-h_features_train(i,k)));
            if(f(closestIndex) == 0)
                closestIndex = 2;
            end
            cdf_vals_h_train(i,k) = f(closestIndex);
        end
        
        for i = 1 : size(h_features_test,1)
            [minValue,closestIndex] = min(abs(x-h_features_test(i,k)));
            if(f(closestIndex) == 0)
                closestIndex = 2;
            end
            cdf_vals_h_test(i,k) = f(closestIndex);
        end
    end

    h_bhd_vals = 1-exp(mean(log(cdf_vals_h_test),2));
    e_bhd_vals = 1-exp(mean(log(cdf_vals_e),2));
    
    [gof_st_ep_ft(cv), gof_st_ep_pat_ft(cv), gof_st_soz_ft(cv), gof_st_soz_pat_ft(cv)] = ...
        performance_metrics(h_bhd_vals, e_bhd_vals, lemon_ids(test_window_ids), mayo_ids, left_ids, right_ids);
end

%% Frontal and Temporal, HA only

h_features = reshape(all_lemon_features_ec(:, 2:3, 4, 1:2), n_neg, 4);
e_features = reshape(all_mayo_features_ec(:, 2:3, 4, 1:2), n_pos, 4);

for cv = 1:10
    rand_ids = unique_lemon_ids(randperm(length(unique_lemon_ids)));
    train_lemon_ids = rand_ids(n_test_healthy+1:end);
    test_lemon_ids = rand_ids(1:n_test_healthy);

    train_window_ids = ismember(lemon_ids, train_lemon_ids);
    test_window_ids = ismember(lemon_ids, test_lemon_ids);

    h_features_train = h_features(train_window_ids, :);
    h_features_test = h_features(test_window_ids, :);
    
    cdf_vals_e = zeros(size(e_features));
    cdf_vals_h_train = zeros(size(h_features_train));
    cdf_vals_h_test = zeros(size(h_features_test));
    for k = 1:size(cdf_vals_h_test,2)
        [f,x] = ecdf(h_features_train(:,k));
        
        for i = 1 : size(e_features,1)
            [minValue,closestIndex] = min(abs(x-e_features(i,k)));
            if(f(closestIndex) == 0)
                closestIndex = 2;
            end
            cdf_vals_e(i,k) = f(closestIndex);
        end
        
        for i = 1 : size(h_features_train,1)
            [minValue,closestIndex] = min(abs(x-h_features_train(i,k)));
            if(f(closestIndex) == 0)
                closestIndex = 2;
            end
            cdf_vals_h_train(i,k) = f(closestIndex);
        end
        
        for i = 1 : size(h_features_test,1)
            [minValue,closestIndex] = min(abs(x-h_features_test(i,k)));
            if(f(closestIndex) == 0)
                closestIndex = 2;
            end
            cdf_vals_h_test(i,k) = f(closestIndex);
        end
    end

    h_bhd_vals = 1-exp(mean(log(cdf_vals_h_test),2));
    e_bhd_vals = 1-exp(mean(log(cdf_vals_e),2));
    
    [gof_st_ep_ft_ha(cv), gof_st_ep_pat_ft_ha(cv), gof_st_soz_ft_ha(cv), gof_st_soz_pat_ft_ha(cv)] = ...
        performance_metrics(h_bhd_vals, e_bhd_vals, lemon_ids(test_window_ids), mayo_ids, left_ids, right_ids);
end

%% plot ROC curves
x_vals = gof_st_ep_ft(1).X';
y_vals_ft = [gof_st_ep_ft(:).Y]';
y_vals_all = [gof_st_ep_all(:).Y]';
y_vals_ft_ha = [gof_st_ep_ft_ha(:).Y]';
figure;
shadedErrorBar(x_vals,mean(y_vals_all,1),2*std(y_vals_all,[],1),'lineprops','-k','transparent',true,'patchSaturation',0.2);
hold on
shadedErrorBar(x_vals,mean(y_vals_ft,1),2*std(y_vals_ft,[],1),'lineprops','-b','transparent',true,'patchSaturation',0.2);
shadedErrorBar(x_vals,mean(y_vals_ft_ha,1),2*std(y_vals_ft_ha,[],1),'lineprops','-r','transparent',true,'patchSaturation',0.2);
hold off
legend('All Features', 'Frontal, Temporal', 'Frontal, Temporal, High Alpha', 'Location','southeast','FontSize',16)
ylim([0 1])
box on
xlabel('False Positive Rate','FontSize',16)
ylabel('True Positivie Rate','FontSize',16)
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none'); findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);

x_vals = gof_st_soz_ft(1).X';
y_vals_ft = [gof_st_soz_ft(:).Y]';
y_vals_all = [gof_st_soz_all(:).Y]';
y_vals_ft_ha = [gof_st_soz_ft_ha(:).Y]';
figure;
shadedErrorBar(x_vals,mean(y_vals_all,1),2*std(y_vals_all,[],1),'lineprops','-k','transparent',true,'patchSaturation',0.2);
hold on
shadedErrorBar(x_vals,mean(y_vals_ft,1),2*std(y_vals_ft,[],1),'lineprops','-b','transparent',true,'patchSaturation',0.2);
shadedErrorBar(x_vals,mean(y_vals_ft_ha,1),2*std(y_vals_ft_ha,[],1),'lineprops','-r','transparent',true,'patchSaturation',0.2);
hold off
legend('All Features', 'Frontal, Temporal', 'Frontal, Temporal, High Alpha', 'Location','southeast','FontSize',16)
ylim([0 1])
box on
xlabel('False Positive Rate','FontSize',16)
ylabel('True Positivie Rate','FontSize',16)
lineWidth = 2; lineCover=3*lineWidth;
a = [findall(gcf,'Marker','none'); findall(gcf,'Marker','.')];
set(a,'LineWidth',lineWidth,'Marker','.','MarkerSize',lineCover);
set(findobj(gcf,'type','axes'),'FontName','Arial','FontSize',16,'FontWeight','Bold', 'LineWidth', 2);

%% generate results table
result_struct = {};
all_gof_structs = {gof_st_ep_pat_all; gof_st_ep_pat_ft; gof_st_ep_pat_ft_ha; gof_st_soz_pat_all; gof_st_soz_pat_ft; gof_st_soz_pat_ft_ha};
for i = 1 : length(all_gof_structs)
    curr_gof_st = all_gof_structs{i};
    all_means = [mean([curr_gof_st(:).AUC]), mean([curr_gof_st(:).precision]), mean([curr_gof_st(:).recall]), mean([curr_gof_st(:).F1])];
    all_stds = [std([curr_gof_st(:).AUC]), std([curr_gof_st(:).precision]), std([curr_gof_st(:).recall]), std([curr_gof_st(:).F1])];
    for j = 1 : length(all_means)
        result_struct{i,j} = sprintf('%.2f (%.2f)', all_means(j), all_stds(j));
    end
end
results_table = cell2table(result_struct, 'VariableNames', {'AUC','Precision','Recall','F1'},'RowNames',...
    {'Healthy Vs. Epilepsy - All', 'Healthy Vs. Epilepsy - FT', 'Healthy Vs. Epilepsy - FT-HA', 'Seizure Focus - All', 'Seizure Focus - FT', 'Seizure Focus - FT-HA'});
% writetable(results_table, 'summary_results.csv','WriteRowNames',true);